module Api
  module V1
    require 'test_helper'
    class GraphControllerTest < ActionDispatch::IntegrationTest
      test 'should get show' do
        get api_v1_graph_url(category: 'sports')
        assert_response :success
        sub_cat_str = 'CULTURE AND SPORTS CULTURE'
        sub_cat = URI.encode(sub_cat_str.force_encoding('ISO-8859-1'))
                     .encode('utf-8', replace: nil).downcase.tr(' ', '_')
        result = [{ 'sub_category' => sub_cat }].to_json
        assert_equal result, @response.body
      end
    end
  end
end
