
module Api
  module V1
    require 'test_helper'
    class CategoryControllerTest < ActionDispatch::IntegrationTest
      test 'should get show' do
        get api_v1_category_url(category: 'sports')
        assert_response :success
        result = {"title":"sports","sub_categories": 1}.to_json
        assert_equal result, @response.body
      end
    end
  end
end
