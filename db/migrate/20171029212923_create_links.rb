class CreateLinks < ActiveRecord::Migration[5.1]
  def up
    create_table :links, id: false do |t|
      t.integer :cl_from
      t.string :cl_to
      t.binary :cl_sortkey
      t.date :cl_timestamp
      t.binary :cl_sortkey_prefix
      t.binary :cl_collation
      t.string :cl_type

      t.timestamps
    end
    execute 'ALTER TABLE links ADD PRIMARY KEY (cl_from)'
  end

  def down
    drop_table :links
  end
end
