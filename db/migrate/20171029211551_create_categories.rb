# Categories Table
# Uses cat_id as id, no generation of id column
class CreateCategories < ActiveRecord::Migration[5.1]
  def up
    create_table :categories, id: false do |t|
      t.integer :cat_id
      t.string :cat_title
      t.integer :cat_pages
      t.integer :cat_subcats
      t.integer :cat_files
      t.timestamps
    end
    execute 'ALTER TABLE categories ADD PRIMARY KEY(cat_id)'
  end
  def down
    drop_table :categories
  end
end
