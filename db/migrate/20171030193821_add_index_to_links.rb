# Adds index on cl_sortkey
class AddIndexToLinks < ActiveRecord::Migration[5.1]
  def up
    add_index :links, :cl_sortkey, length: 200
  end

  def down
    remove_index :links, :cl_sortkey
  end
end
