Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get '/graph/:category', to: 'graph#show', as: 'graph'
    end
  end

  namespace :api do
    namespace :v1 do
      get '/category/:category', to: 'category#show', as: 'category'
    end
  end
end
