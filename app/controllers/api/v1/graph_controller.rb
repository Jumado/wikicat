module Api
  module V1
    # Subcategory graphs controller
    class GraphController < ApplicationController
      def show
        cat = params[:category] || 'sports'
        @category = Category.where(cat_title: cat.capitalize).first
        respond_to_show
      end

      private

      def graph_params
        params.require(:category).permit(:cl_to, :cl_type)
      end

      def respond_to_show
        if @category
          @links = Link.where(cl_to: @category.cat_title,
                              cl_type: 'subcat')
          render json: @links,
                 each_serializer: LinkSerializer,
                 root: @category.cat_title.downcase
        else
          render json: { error: { text: '404 not found', status: 404 } }
        end
      end
    end
  end
end
