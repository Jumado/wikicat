module Api
  module V1
    # Category controller v1
    class CategoryController < ApplicationController
      def show
        category = params[:category] ? params[:category] : 'sports'
        @category = Category.where(cat_title: category.capitalize).first
        if @category
          render json: @category,
                 serializer: CategorySerializer,
                 root: 'category'
        else
          render json: { error: { text: '404 Not found', status: 404 } }
        end
      end
    end
  end
end
